# Installation
> `npm install --save @types/emscripten`

# Summary
This package contains type definitions for emscripten (https://emscripten.org).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/emscripten.

### Additional Details
 * Last updated: Thu, 23 Jan 2025 18:02:27 GMT
 * Dependencies: none

# Credits
These definitions were written by [Kensuke Matsuzaki](https://github.com/zakki), [Periklis Tsirakidis](https://github.com/periklis), [Bumsik Kim](https://github.com/kbumsik), and [Louis DeScioli](https://github.com/lourd).
