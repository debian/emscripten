Source: emscripten
Section: devel
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@alioth-lists.debian.net>
Uploaders: Jérémy Lal <kapouer@melix.org>
Build-Depends:
 binaryen (>= 120~) <!nocheck>,
 ccache,
 clang-19 <!nocheck>,
 cmake <!nocheck>,
 cmark-gfm <!nodoc>,
 debhelper-compat (= 13),
 dh-sequence-python3,
 esbuild <!nocheck>,
 fonts-liberation <!nocheck>,
 help2man,
 htmlmin <!nocheck>,
 jq,
 libjs-source-map,
 libsimde-dev,
 lld-19 <!nocheck>,
 llvm-19 <!nocheck>,
 node-acorn <!nocheck>,
 node-source-map <!nocheck>,
 nodejs (>= 12.16) <!nocheck>,
 dh-nodejs <!nocheck>,
 python3,
 python3-numpy <!nocheck>,
 python3-ply <!nocheck>,
 scons <!nocheck>,
 wabt (>= 1.0.24-2~) <!nocheck>,
Build-Depends-Indep:
 dh-sequence-sphinxdoc <!nodoc>,
 python3-doc <!nodoc>,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
Standards-Version: 4.7.0
Homepage: http://emscripten.org/
Vcs-Browser: https://salsa.debian.org/debian/emscripten
Vcs-Git: https://salsa.debian.org/debian/emscripten.git
Rules-Requires-Root: no

Package: emscripten
Architecture: all
Depends:
 binaryen,
 ccache,
 clang-19,
 lld-19,
 llvm-19,
 node-acorn,
 nodejs (>= 12),
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 libjs-d3,
 python3-numpy,
Suggests:
 adb,
 automake,
 closure-compiler,
 cmake,
 emscripten-doc,
 make,
 python3-ply,
 scons,
 wabt (>= 1.0.24-2~),
Provides:
 node-types-emscripten (= ${types:Version}),
Description: LLVM-to-JavaScript Compiler
 Emscripten is an LLVM to JavaScript compiler. It takes LLVM bitcode, also
 called LLVM IR (which can be generated from C/C++ using Clang, or any other
 language that can be converted into LLVM bitcode) and compiles that into
 JavaScript, which can be run on the web (or anywhere else JavaScript can run).
 .
 Using Emscripten, you can
   * Compile C and C++ code into JavaScript and run that on the web
   * Run code in languages like Python as well, by compiling CPython from C
     to JavaScript and interpreting code in that on the web
 .
 Some uses of emscripten require additional packages:
  * setting WASM2C requires wabt (at least release 1.0.24-2).
  * emcmake requires cmake.
  * emconfigure may require automake.
  * emmake requires make.
  * emrun option --android requires adb.
  * emscons requires scons.
  * WebIDL Binder requires python3-ply.

Package: emscripten-doc
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
 python3-doc,
Built-Using:
 ${sphinxdoc:Built-Using},
Architecture: all
Description: LLVM-to-JavaScript Compiler (documentation)
 Emscripten is an LLVM to JavaScript compiler. It takes LLVM bitcode, also
 called LLVM IR (which can be generated from C/C++ using Clang, or any other
 language that can be converted into LLVM bitcode) and compiles that into
 JavaScript, which can be run on the web (or anywhere else JavaScript can run).
 .
 Using Emscripten, you can
   * Compile C and C++ code into JavaScript and run that on the web
   * Run code in languages like Python as well, by compiling CPython from C
     to JavaScript and interpreting code in that on the web
 .
 This package contains the documentation.
